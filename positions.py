from dataclasses import dataclass

@dataclass
class WorkBookPosition:
    row_coord: int
    column_corrd: int

configuration_number_pos = WorkBookPosition(2, 4)
number_of_boxes_pos = WorkBookPosition(2, 14)
spacing_column_position = WorkBookPosition(4, 8)
array_information_pos = WorkBookPosition(26, 4)
first_delay_of_boxes_pos = WorkBookPosition(5, 25)
last_delay_of_boxes_pos = WorkBookPosition(16, 25)

report_array_information_pos_first_cell = WorkBookPosition(1, 1)
report_array_information_pos_second_cell = WorkBookPosition(1, 2)
report_speaker_pair_num_label_pos = WorkBookPosition(2, 1)
report_speaker_pair_array_delay_label_pos = WorkBookPosition(2, 2)
report_speaker_pair_user_delay_label_pos = WorkBookPosition(2, 3)
report_speaker_pair_sum_delay_label_pos = WorkBookPosition(2, 4)
