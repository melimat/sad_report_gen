from math import ceil

from SAD_reader import *
from Report_writer import *

def generateDelayReportArray(delay_array: list, num_of_boxes: int) -> list:
    result = []
    mid_point = ceil(float(num_of_boxes) / 2)

    for i in range(mid_point):
        result.append(delay_array[i])
        print(delay_array[i])
    
    return result

input_path_str = "array_1.xlsm"
output_path_str = "array_report.xlsx"

reader = SAD_reader(input_path_str)
writer = Report_witer()

delay_array = reader.getDelayArr()
num_of_boxes = reader.getNumOfBoxes()
array_angle = reader.getArrayAngle()
array_spacing = reader.getSpacing()

writer.setAngle(array_angle)
writer.setSpacing(array_spacing)

pair_delay_arr = generateDelayReportArray(delay_array, num_of_boxes)
writer.setDelayArr(pair_delay_arr)
writer.generateReport(output_path_str)