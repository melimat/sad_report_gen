from array import array
from warnings import catch_warnings
import openpyxl

import positions

class SAD_reader:
    def __init__(self, path: str):
        self.path = path
        self.__readAllSpeakerAttributes()
    def __readAllSpeakerAttributes(self):
        try:
            sad_workbook = openpyxl.load_workbook(self.path, data_only=True)
            sad_sheet = sad_workbook.active

            self.configuration_number = sad_sheet.cell(
                positions.configuration_number_pos.row_coord,
                positions.configuration_number_pos.column_corrd
            ).value

            self.number_of_boxes = sad_sheet.cell(
                positions.number_of_boxes_pos.row_coord,
                positions.number_of_boxes_pos.column_corrd
            ).value

            self.array_info = sad_sheet.cell(
                positions.array_information_pos.row_coord,
                positions.array_information_pos.column_corrd
            ).value

            self.spacing = sad_sheet.cell(
                positions.spacing_column_position.row_coord,
                positions.spacing_column_position.column_corrd
            ).value

            self.delay_arr = []

            for i in range(
                positions.first_delay_of_boxes_pos.row_coord,
                positions.first_delay_of_boxes_pos.row_coord + self.number_of_boxes
            ):
                delay_cell = sad_sheet.cell(i, positions.first_delay_of_boxes_pos.column_corrd)
                self.delay_arr.append(float(delay_cell.value))
                #print(delay_cell.value)

        except Exception as e:
            raise e

    def getDelayArr(self) -> list:
        return self.delay_arr
    
    def getNumOfBoxes(self) -> int:
        return self.number_of_boxes
    
    def getArrayAngle(self) -> int:
        second_part = self.array_info.split("(")[1]
        num_str = second_part.split("°")[0]
        return int(num_str)
    
    def getSpacing(self) -> float:
        return float(self.spacing)
