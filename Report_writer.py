import openpyxl

import positions

class Report_witer:
    def __init__(self):
        self.spacing = float()
        self.angle = int()
        self.delay_arr = []
    
    def setSpacing(self, spacing: float ):
        self.spacing = spacing
    
    def setAngle(self, angle: int):
        self.angle = angle
    
    def setDelayArr(self, arr: list):
        self.delay_arr = arr

    def __generateInfoString(self) -> str:
        output_str = "Arc: {0}deg ; spacing: {1}m".format(self.angle, self.spacing)
        return output_str
    
    def generateReport(self, report_path: str):
        output_workbook = openpyxl.Workbook()
        sheet = output_workbook.active

        #Info field
        sheet.merge_cells(
            start_row=positions.report_array_information_pos_first_cell.row_coord,
            start_column=positions.report_array_information_pos_first_cell.column_corrd,
            end_row=positions.report_array_information_pos_second_cell.row_coord, 
            end_column=positions.report_array_information_pos_second_cell.column_corrd
        )
        info_cell = sheet.cell(
            positions.report_array_information_pos_first_cell.row_coord,
            positions.report_array_information_pos_first_cell.column_corrd
        )
        info_cell.value = self.__generateInfoString()

        #Header for delay table
        num_label_cell = sheet.cell(
            positions.report_speaker_pair_num_label_pos.row_coord,
            positions.report_speaker_pair_num_label_pos.column_corrd
        )
        num_label_cell.value = "Speaker pair"

        array_delay_label_cell = sheet.cell(
            positions.report_speaker_pair_array_delay_label_pos.row_coord,
            positions.report_speaker_pair_array_delay_label_pos.column_corrd
        )
        array_delay_label_cell.value = "Array delay"

        user_delay_label_cell = sheet.cell(
            positions.report_speaker_pair_user_delay_label_pos.row_coord,
            positions.report_speaker_pair_user_delay_label_pos.column_corrd
        )
        user_delay_label_cell.value = "User delay"

        sum_delay_label_cell = sheet.cell(
            positions.report_speaker_pair_sum_delay_label_pos.row_coord,
            positions.report_speaker_pair_sum_delay_label_pos.column_corrd
        )
        sum_delay_label_cell.value = "Sum"

        #Generating the table
        for index, delay in enumerate(reversed(self.delay_arr)):
            seq_num = index + 1

            #Pair number
            pair_num_cell = sheet.cell(
                positions.report_speaker_pair_num_label_pos.row_coord + seq_num,
                positions.report_speaker_pair_num_label_pos.column_corrd
            )
            pair_num_cell.value = seq_num

            #Array delay
            delay_cell = sheet.cell(
                positions.report_speaker_pair_array_delay_label_pos.row_coord + seq_num,
                positions.report_speaker_pair_array_delay_label_pos.column_corrd
            )
            delay_cell.number_format = '0.00'
            delay_cell.value = delay

            #User delay
            user_delay_cell = sheet.cell(
                positions.report_speaker_pair_user_delay_label_pos.row_coord + seq_num,
                positions.report_speaker_pair_user_delay_label_pos.column_corrd
            )
            user_delay_cell.number_format = '0.00'
            user_delay_cell.value = 0

            #Sum delay
            sum_delay_cell = sheet.cell(
                positions.report_speaker_pair_sum_delay_label_pos.row_coord + seq_num,
                positions.report_speaker_pair_sum_delay_label_pos.column_corrd
            )
            sum_delay_cell.number_format = '0.00'
            sum_delay_cell.value = "= {0} + {1}".format(delay_cell.coordinate, user_delay_cell.coordinate)
        
        output_workbook.save(report_path)
